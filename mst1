/*
 * A. MST-1
 * Требуется найти в связном графе остовное дерево минимального веса.
 * Воспользуйтесь алгоритмом Прима.
 */
#include <iostream>
#include <vector>
#include <set>
#include <limits>

using namespace std;

const int INF = numeric_limits<int>::max();

class Graph {
public:
    explicit Graph(int new_size);

    void addEdge(int from, int to, int weight = 0);

    int getWeight(int from, int to) const;

    vector<pair<int, int>> getNeighbours(int v) const { return _g[v]; };

private:
    vector<vector<pair<int, int>>> _g; // _g[from] = pair<to, cost>
};

Graph::Graph(int new_size) {
    _g.resize(new_size + 1);
}

void Graph::addEdge(int from, int to, int weight) {
    _g[from].push_back(make_pair(to, weight));
    _g[to].push_back(make_pair(from, weight));
}

int Graph::getWeight(int from, int to) const {
    int result = INF;
    for (auto item : _g[from]) {
        if (item.first == to) {
            result = min(result, item.second);
        }
    }
    return result;
}

int FindMST(const Graph& g, int n) {
    // uses Prima algorithm
    int result = 0;
    vector<bool> used(n + 1);
    vector<int> min(n + 1, INF);
    vector<int> sel(n + 1, -1);
    min[1] = 0;
    set<pair<int, int>> q;
    q.insert(make_pair(0, 1));
    for (int i = 1; i <= n; ++i) {
        int v = q.begin()->second;
        q.erase(q.begin());

        if (sel[v] != -1) {
            result += g.getWeight(v, sel[v]);
        }
        vector<pair<int, int>> neighbours = g.getNeighbours(v);
        for (auto item : neighbours) {
            int to = item.first;
            int cost = item.second;
            if (cost < min[to]) {
                q.erase(make_pair(min[to], to));
                min[to] = cost;
                sel[to] = v;
                q.insert(make_pair(min[to], to));
            }
        }
    }
    return result;
}

int main() {
    int n, m;
    cin >> n >> m;
    Graph g = Graph(n);
    for (int i = 1; i <= m; ++i) {
        int from, to, weight;
        cin >> from >> to >> weight;
        g.addEdge(from, to, weight);
    }
    cout << FindMST(g, n);
    return 0;
}
