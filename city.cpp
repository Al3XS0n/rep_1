/*
 * C. Районы, кварталы, жилые массивы
 * Фрэнку опять прилетел новый заказ. Однако в этот раз город играет по очень странным
 * правилам: все дороги в городе односторонние и связывают только офисы нанимателей
 * перевозчика. Множество офисов, между любыми двумя из которых существует путь, образуют
 * квартал, если нельзя добавить никакие другие, чтобы условие выполнялось. Фрэнку
 * интересно, каково минимальное количество односторонних дорог нужно ещё построить, чтобы
 * весь город стал квраталом.
 */
#include <iostream>
#include <vector>
#include <limits>

using namespace std;

const int INF = numeric_limits<int>::max();

class Graph {
public:
    Graph() : _orient(false) {}

    Graph(size_t size, bool orient = false) : _orient(orient) { _g.resize(size + 1); }

    ~Graph() { _g.clear(); }

    void AddEdge(int from, int to);

    vector<int> GetNeighbours(int v) const { return _g[v]; }

private:
    bool _orient;
    vector<vector<int>> _g; // g[from][i] = to;
};

void Graph::AddEdge(int from, int to) {
    _g[from].push_back(to);
    if (!_orient) {
        _g[to].push_back(from);
    }
}

void IsHaveChildParent(int v, const Graph& g, vector<bool>& used, vector<int>& col,
           vector<bool>& haveChild, vector<bool>& haveParent) {
    // проверка вершин графа с использованием обхода в глубину
    used[v] = 1;
    vector<int> neighbours = g.GetNeighbours(v);
    for (auto u : neighbours) {
        if (col[u] != col[v]) {
            haveChild[col[v]] = true;
            haveParent[col[u]] = true;
        }
        if (!used[u]) {
            IsHaveChildParent(u, g, used, col, haveChild, haveParent);
        }
    }
}

void DFS(int v, const Graph& g, vector<bool>& used, vector<int>& order) {
    used[v] = true;
    vector<int> neighbours = g.GetNeighbours(v);
    for (auto u : neighbours) {
        if (!used[u]) {
            DFS(u, g, used, order);
        }
    }
    order.push_back(v);
}

void GetSCC(int v, const Graph& gReverse, vector<bool>& used, vector<int>& comp) {
    used[v] = true;
    comp.push_back(v);
    vector<int> neighbours = gReverse.GetNeighbours(v);
    for (auto u : neighbours) {
        if (!used[u]) {
            GetSCC(u, gReverse, used, comp);
        }
    }
}

void FindSCC(int n, const Graph& g, const Graph& gReverse) {
    int countOfSCC = 0;
    vector<bool> used, haveParent, haveChild;
    vector<int> order, comp, col;
    col.resize(n + 1, 0);
    used.resize(n + 1, false);
    for (int i = 1; i <= n; ++i) {
        if (!used[i]) {
            DFS(i, g, used, order);
        }
    }
    used.assign(n + 1, false);
    for (int i = 1; i <= n; ++i) {
        int v = order[n - i];
        if (!used[v]) {
            GetSCC(v, gReverse, used, comp);
            countOfSCC++;
            for (auto u : comp) {
                col[u] = countOfSCC;
            }
            comp.clear();
        }
    }

    have_child.resize(countOfSCC + 1, false);
    have_parent.resize(countOfSCC + 1, false);
    used.assign(n + 1, false);
    for (int i = 1; i <= n; ++i) {
        if (!used[i]) {
            IsHaveChildParent(i, g, used, col, haveChild, haveParent);
        }
    }
    int cnt1 = 0, cnt2 = 0;
    for (int i = 1; i <= countOfSCC; ++i) {
        cnt1 += !(haveChild[i]);
        cnt2 += !(haveParent[i]);
    }
    if (countOfSCC == 1) {
        cout << 0;
    } else {
        cout << max(cnt1, cnt2);
    }
}

int main() {
    int n, m;
    cin >> n >> m;
    Graph g(n, true);
    Graph gReverse(n, true);
    for (int i = 1; i <= m; ++i) {
        int u, v;
        cin >> u >> v;
        if (u == v) {
            continue;
        }
        g.AddEdge(u, v);
        gReverse.AddEdge(v, u);
    }

    FindSCC(n, g, gReverse);
    return 0;
}
