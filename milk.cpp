/*
 * A. За молоком
 * Леон и Матильда собрались пойти в магазин за молоком.
 * Их хочет поймать Стэнсфилд, поэтому нашим товарищам нужно сделать это как можно быстрее.
 * Каково минимальное количество улиц, по которым пройдёт
 * хотя бы один из ребят (либо Матильда, либо Леон, либо оба вместе)?
 */
#include <iostream>
#include <vector>
#include <queue>
#include <limits>

using namespace std;

const int INF = numeric_limits<int>::max();

class Graph {
public:
    Graph() : _orient(false) {}

    Graph(size_t size, bool orient = false) : _orient(orient) { _g.resize(size + 1); }

    void add_edge(int from, int to);

    vector<int> get_neighbours(int v) const { return _g[v]; }

private:
    bool _orient;
    vector<vector<int>> _g; // g[from][i] = to;
};

void Graph::add_edge(int from, int to) {
    _g[from].push_back(to);
    if (!_orient) {
        _g[to].push_back(from);
    }
}

void BFS(queue<int> query, const Graph& g, vector<int>& dist) {
    while (!query.empty()) {
        int v = query.front();
        query.pop();
        vector<int> neighbours = g.get_neighbours(v);
        for (auto u : neighbours) {
            if (dist[u] == 0) {
                dist[u] = dist[v] + 1;
                query.push(u);
            }
        }
    }
}

void Calc_Dist(int n, int start, const Graph& g, vector<int>& dist) {
    queue<int> q;
    dist.assign(n + 1, 0);
    q.push(start);
    dist[start] = 1;
    BFS(q, g, dist);
    return;
}

int Find_Path(int n, const Graph& g, int leon, int matilda, int milk) {
    vector<int> dist_leon, dist_matilda, dist_milk;

    Calc_Dist(n, leon, g, dist_leon);
    Calc_Dist(n, matilda, g, dist_matilda);
    Calc_Dist(n, milk, g, dist_milk);

    int result = INF;
    for (int i = 1; i <= n; ++i) {
        result = min(result, dist_leon[i] + dist_matilda[i] + dist_milk[i] - 3);
    }
    return result;
}


int main() {
    int n, m, leon, matilda, milk;

    cin >> n >> m >> leon >> matilda >> milk;
    Graph g = Graph(n);
    for (int i = 1; i <= m; ++i) {
        int u, v;
        cin >> u >> v;
        g.add_edge(u, v);
    }

    cout << Find_Path(n, g, leon, matilda, milk);
    return 0;
}
