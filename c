/*
 * C. Художник и фотограф
 * У художника-авангардиста есть полоска разноцветного холста. За один раз он перекрашивает
 * некоторый отрезок полоски в некоторый цвет. После каждого перекрашивания специально
 * обученный фотограф делает снимок части получившегося творения для музея современного
 * искусства. Для правильного экспонирования требуется знать яркость самого темного цвета на
 * выбранном фотографом отрезке. Напишите программу для определения яркости самого
 * темного цвета на отрезке.
 */
#include <iostream>
#include <vector>

using namespace std;

class SegmentTree {
public:
    int getMin(int v, int tl, int tr, int l, int r);

    void update(int v, int tl, int tr, int l, int r, int val);

    explicit SegmentTree(const vector<int>& array);

private:
    vector<int> _tree;

    vector<int> _temp;// несогласованность вершины

private:
    void _build(const vector<int>& array, int v, int tl, int tr);

    void _push(int v);
};

void SegmentTree::_build(const vector<int>& array, int v, int tl, int tr) {
    if (tl == tr) {
        _tree[v] = array[tl];
        return;
    }
    int tm = (tl + tr) / 2;
    _build(array, v + v, tl, tm);
    _build(array, v + v + 1, tm + 1, tr);
    _tree[v] = min(_tree[v + v], _tree[v + v + 1]);
}

int SegmentTree::getMin(int v, int tl, int tr, int l, int r) {
    if (tr < l || tl > r) {
        return 2000;
    }
    _push(v);
    if (l <= tl && tr <= r) {
        return _tree[v];
    }
    int tm = (tl + tr) / 2;
    return min(getMin(v + v, tl, tm, l, r), getMin(v + v + 1, tm + 1, tr, l, r));
}

void SegmentTree::_push(int v) {
    if (_temp[v] == -1) {
        return;
    }
    int curTemp = _temp[v];
    _temp[v] = -1;
    _temp[v + v] = _temp[v + v + 1] = curTemp;
    _tree[v + v] = _tree[v + v + 1] = curTemp;
}

void SegmentTree::update(int v, int tl, int tr, int l, int r, int val) {
    if (tl > r || tr < l) {
        return;
    }
    _push(v);
    if (l <= tl && tr <= r) {
        _tree[v] = val;
        _temp[v] = val;
        return;
    }
    int tm = (tl + tr) / 2;
    update(v + v, tl, tm, l, r, val);
    update(v + v + 1, tm + 1, tr, l, r, val);
    _tree[v] = min(_tree[v + v], _tree[v + v + 1]);
}

SegmentTree::SegmentTree(const vector<int>& array) {
    int n = array.size();
    _tree.resize(8 * (n + 1));
    _temp.assign(8 * (n + 1), -1);
    _build(array, 1, 1, n);
}

int main() {
    int n, m;
    vector<int> a;
    cin >> n;
    a.resize(n + 1);
    for (int i = 1; i <= n; ++i) {
        int r, g, b;
        cin >> r >> g >> b;
        a[i] = r + g + b;
    }
    SegmentTree tree(a);
    cin >> m;
    for (int i = 1; i <= m; ++i) {
        int c, d, r, g, b, e, f;
        cin >> c >> d >> r >> g >> b >> e >> f;
        int brightness = r + g + b;
        tree.update(1, 1, n, c + 1, d + 1, brightness);
        cout << tree.getMin(1, 1, n, e + 1, f + 1) << ' ';
    }
    return 0;
}