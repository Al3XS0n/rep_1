/*
 * B. Секретные материалы
 * Джон Макклейн сообщает по рации новую информацию о террористах в отдел с n
 * полицейскими. Он звонит нескольким сотрудникам и просит распространить информацию по
 * отделу, зная, что у каждого полицейского есть связь с определёнными коллегами. Джон
 * Макклейн хочет, чтобы операция прошла успешно. Но если полицейский позвонит коллеге, от
 * которого(возможно, не напрямую) сам получил информацию, террористы смогут отследить
 * этот звонок и помешать операции. Если же двое сотрудников оповестят одного, ничего плохого
 * не произойдёт. Помогите Джону Макклейну. Выведите NO, если кто-то из
 * полицейских ошибётся, делая звонок. Если всё пройдёт хорошо, выведите YES и порядок, в котором
 * полицейские получат информацию, считая, что полицейские оповещают коллег по
 * возрастанию их номеров, а в начале Джон даёт информацию тем, кому не может позвонить
 * никто из коллег.
 */
#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

const int INF = numeric_limits<int>::max();

class Graph {
public:
    Graph() : _orient(false) {}

    Graph(size_t size, bool orient = false) : _orient(orient) { _g.resize(size + 1); }

    void AddEdge(int from, int to);

    vector<int> GetNeighbours(int v) const { return _g[v]; }

private:
    bool _orient;
    vector<vector<int>> _g; // g[from][i] = to;
};

void Graph::AddEdge(int from, int to) {
    _g[from].push_back(to);
    if (!_orient) {
        _g[to].push_back(from);
    }
}

void TopSort(int v, const Graph& g, vector<int>& sorted, vector<int>& used) {
    used[v] = 1;
    bool f = false;
    vector<int> neighbours = g.GetNeighbours(v);
    for (auto u : neighbours) {
        if (!used[u]) {
            TopSort(u, g, sorted, used);
        }
    }
    sorted.push_back(v);
}

bool IsCycle(int v, const Graph& g, vector<int>& used) {
    used[v] = 1;
    bool f = false;
    vector<int> neighbours = g.GetNeighbours(v);
    for (auto u : neighbours) {
        if (used[u] == 1) {
            return true;
        }
        if (used[u] == 0) {
            f = f || IsCycle(u, g, used);
        }
    }
    used[v] = 2;
    return f;
}

void FindOrder(int n, const Graph& g, vector<int>& ans) {
    vector<int> used;
    used.assign(n, 0);

    for (int i = 0; i < n; ++i) {
        if (!used[i] && IsCycle(i, g, used)) {
            cout << "NO";
            exit(0);
        }
    }
    used.assign(n, 0);

    for (int i = 0; i < n; ++i) {
        if (!used[i]) {
            TopSort(i, g, ans, used);
        }
    }
    reverse(ans.begin(), ans.end());
}

int main() {
    int n, m;
    vector<int> ans;

    cin >> n >> m;
    Graph g(n, true);
    for (int i = 0; i < m; ++i) {
        int u, v;
        cin >> u >> v;
        g.AddEdge(u, v);
    }

    FindOrder(n, g, ans);

    cout << "YES\n";
    for (auto ver : ans) {
        cout << ver << ' ';
    }
    return 0;
}
