/*
 * C. Wake Me Up When Rickember Ends
 * Сейчас Рику надо попасть из вселенной с номером S во вселенную с номером F. Он знает все
 * существующие телепорты, и казалось бы нет никакой проблемы. Но, далеко не секрет, что за
 * свою долгую жизнь Рик поссорился много с кем. Из своего личного опыта он знает, что при
 * телепортациях есть вероятность, что его заставят ответить за свои слова.
 * Если Рик знает вероятности быть прижатым к стенке на всех существующих телепортациях,
 * помогите ему посчитать минимальную вероятность, что он всё-таки столкнется с
 * неприятностями.
 */
#include <iostream>
#include <vector>
#include <limits>
#include <iomanip>

const int INF = std::numeric_limits<int>::max();

class Graph {
public:
    Graph() : _countOfVertex(0), _orient(false) {}

    explicit Graph(int countOfVertex, bool orient = false) : _countOfVertex(countOfVertex), _orient(orient) {
        _edge = std::vector<std::vector<std::pair<int, double>>>(countOfVertex + 1);
    }

    void addEdge(int from, int to, double cost);

    std::vector<std::pair<int, double>> getNeighbours(int vertex) const {
        return _edge[vertex];
    }

    int getSize() const {
        return _countOfVertex;
    }

private:
    int _countOfVertex;
    bool _orient;
    std::vector <std::vector<std::pair<int, double>>> _edge; // g[v][i] <-> pair(to, weight)
};

void Graph::addEdge(int from, int to, double cost) {
    _edge[from].push_back(std::make_pair(to, cost));
    if (!_orient) {
        _edge[to].push_back(std::make_pair(from, cost));
    }
}

double FindShortestPath(const Graph& g, int start, int finish) {
    std::vector<std::vector<double>> dist(g.getSize() + 1);
    for (int i = 0; i <= g.getSize(); ++i) {
        dist[i] = std::vector<double>(g.getSize() + 1, 0);
    }
    dist[0][start] = 1;
    for (int i = 0; i < g.getSize(); ++i) {
        std::vector<std::pair<int, double>> vn;
        for (int v = 1; v <= g.getSize(); ++v) {
            vn = g.getNeighbours(v);
            for (auto u : vn) {
                int to = u.first;
                double cost = u.second;
                dist[i + 1][to] = std::max(dist[i + 1][to], dist[i][v] * cost);
            }
        }
    }
    double res = 0;
    for (int i = 0; i <= g.getSize(); ++i) {
        res = std::max(res, dist[i][finish]);
    }
    return res;
}

int main() {
    int n, m, s, f;
    std::cin >> n >> m >> s >> f;
    Graph g(n);
    for (int i = 1; i <= m; ++i) {
        int from, to;
        double cost;
        std::cin >> from >> to >> cost;
        g.addEdge(from, to, 1 - cost / 100);
    }
    std::cout << 1 - FindShortestPath(g, s, f);
}