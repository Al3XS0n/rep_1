/*
 * A. Вторая статистика
 * Дано число N и последовательность из N целых чисел. Найти вторую порядковую статистику
 * на заданных диапазонах.
 * Для решения задачи используйте структуру данных Sparse Table. Требуемое время обработки
 * каждого диапазона O(1). Время подготовки структуры данных O(n log(n)).
 */
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class SparseTable {
public:
    explicit SparseTable(const vector<int>& array);

    int getMin(int l, int r) const;
private:
    void _init(const vector<int>& array);
private:
    int n;
    vector<vector<int>> _table;
    vector<int> _data;
};

SparseTable::SparseTable(const vector<int>& array) : n(array.size()), _data(array) {
    _init(array);
}

void SparseTable::_init(const vector<int>& array) {
    _table.resize(n);
    for (int i = 0; i < n; ++i) {
        _table[i].resize(20);
    }
    for (int i = 0; i < n; i++) {
        _table[i][0] = i;
    }
    for (int j = 1; (1 << j) <= n; j++) {
        for (int i = 0; (i + (1 << j) - 1) < n; i++) {
            if (_data[_table[i][j - 1]] < _data[_table[i + (1 << (j - 1))][j - 1]]) {
                _table[i][j] = _table[i][j - 1];
            } else {
                _table[i][j] = _table[i + (1 << (j - 1))][j - 1];
            }
        }
    }
}

int SparseTable::getMin(int l, int r) const {
    int j = (int) log2(r - l + 1);

    if (_data[_table[l][j]] <= _data[_table[r - (1 << j) + 1][j]]) {
        return _table[l][j];
    } else {
        return _table[r - (1 << j) + 1][j];
    }
}

int GetSecondStatistic(const SparseTable& st, vector<int>& array, int l, int r) {
    l--, r--;
    int minPos = st.getMin(l, r);
    if (l == minPos) {
        return st.getMin(l + 1, r);
    } else if (r == minPos) {
        return st.getMin(l, r - 1);
    } else {
        int minPos1 = st.getMin(l, minPos - 1);
        int minPos2 = st.getMin(minPos + 1, r);
        if (array[minPos1] <= array[minPos2]) {
            return minPos1;
        } else {
            return minPos2;
        }
    }
}


int main() {
    int n, m;
    vector<int> a;
    cin >> n >> m;
    a.assign(n, 0);
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    SparseTable st(a);
    for (int i = 0; i < m; ++i) {
        int l, r;
        cin >> l >> r;
        cout << a[GetSecondStatistic(st, a, l, r)] << endl;
    }
    return 0;
}