/*
 * A. Who let the Rick out?
 * Рик и Морти снова бороздят просторы вселенных, но решили ограничиться только теми,
 * номера которых меньше M. Они могут телепортироваться из вселенной с номером z во
 * вселенную (z+1) mod M за a бутылок лимонада или во вселенную (z2+1) mod M за b бутылок
 * лимонада. Рик и Морти хотят добраться из вселенной с номером x во вселенную с номером y.
 * Сколько бутылок лимонада отдаст Рик за такое путешествие, если он хочет потратить их как
 * можно меньше?
 */
#include <iostream>
#include <vector>
#include <set>
#include <limits>

const int INF = std::numeric_limits<int>::max();

class Graph {
public:
    Graph() : _countOfVertex(0), _orient(false) {}

    explicit Graph(int countOfVertex, bool orient = false) : _countOfVertex(countOfVertex), _orient(orient) {
        _edge = std::vector<std::vector<std::pair<int, int>>>(countOfVertex + 1);
    }

    void addEdge(int from, int to, int cost);

    std::vector<std::pair<int, int>> getNeighbours(int vertex) const {
        return _edge[vertex];
    }

    int getSize() const {
        return _countOfVertex;
    }

private:
    int _countOfVertex;
    bool _orient;
    std::vector <std::vector<std::pair<int, int>>> _edge; // g[v][i] <-> pair(to, weight)
};

void Graph::addEdge(int from, int to, int cost) {
    _edge[from].push_back(std::make_pair(to, cost));
    if (!_orient) {
        _edge[to].push_back(std::make_pair(from, cost));
    }
}

int FindShortestPath(const Graph& g, int start, int finish) {
    // Uses Dijkstra algorithm
    std::set<std::pair<int,int>> q;
    std::vector<int> dist(g.getSize() + 1, INF);
    dist[start] = 0;
    q.insert(std::make_pair(dist[start], start));
    while (!q.empty()) {
        int v = q.begin()->second;
        q.erase(q.begin());
        if (v == finish) {
            break;
        }
        std::vector<std::pair<int,int>> neighbours = g.getNeighbours(v);
        for (auto u : neighbours) {
            int to = u.first, cost = u.second;
            if (dist[to] > dist[v] + cost) {
                q.erase(std::make_pair(dist[to], to));
                dist[to] = dist[v] + cost;
                q.insert(std::make_pair(dist[to], to));
            }
        }
    }
    return dist[finish];
}

int main() {
    int a, b, m, x, y;
    std::cin >> a >> b >> m >> x >> y;
    Graph g(m, true);
    for (int i = 0; i < m; ++i) {
        g.addEdge(i, (i + 1) % m, a);
        g.addEdge(i, static_cast<int>((1LL * i * i + 1) % m), b);
    }
    std::cout << FindShortestPath(g, x, y);
}