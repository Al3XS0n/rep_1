/*
 * D. Look What You Made Rick Do
 * Рик отправляет Морти в путешествие по N вселенным. У него есть список всех существующих
 * однонаправленных телепортов. Чтобы Морти не потерялся, Рику необходимо узнать, между какими
 * вселенными существуют пути, а между какими нет. Помогите ему в этом!
 */
#include <iostream>
#include <vector>
#include <limits>
#include <queue>
#include <cstring>

const int INF = std::numeric_limits<int>::max();

class Graph {
public:
    Graph() : _countOfVertex(0), _orient(false) {}

    explicit Graph(int countOfVertex, bool orient = false) : _countOfVertex(countOfVertex), _orient(orient) {
        _edge = std::vector<std::vector<int>>(countOfVertex + 1);
    }

    void addEdge(int from, int to);

    const std::vector<int>& getNeighbours(int vertex) const {
        return _edge[vertex];
    }

    int getSize() const {
        return _countOfVertex;
    }

private:
    int _countOfVertex;
    bool _orient;
    std::vector <std::vector<int>> _edge; // g[v][i] <-> neighbour
};

void Graph::addEdge(int from, int to) {
    _edge[from].push_back(to);
    if (!_orient) {
        _edge[to].push_back(from);
    }
}

void BuildBridges(const Graph& g, int sz) {
    std::vector<std::vector<bool>> res(sz + 1, std::vector<bool>(sz + 1));
    std::queue<int> q;
    std::vector<int> neighbours;
    for (int i = 1; i <= sz; ++i) {
        q.push(i);
        res[i] = std::string(sz, '0');
        while (!q.empty()) {
            int v = q.front();
            q.pop();
            neighbours = g.getNeighbours(v);
            for (auto to : neighbours) {
                if (!res[i][to - 1]) {
                    res[i][to - 1] = true;
                    if (to < i) {
                        for (int j = 1; j <= sz; ++j) {
                            if (res[to][j - 1]) {
                                res[i][j - 1] = true;
                            }
                        }
                        continue;
                    }
                    if (to != i) {
                        q.push(to);
                    }
                }
            }
        }
    }
    for (int i = 1; i <= sz; ++i) {
        for (int j = 1; j <= sz; ++j) {
            std::cout << res[i][j];
        }
        std::cout << endl;
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    int n;
    std::cin >> n;
    Graph g(n, true);
    for (int i = 1; i <= n; ++i) {
        std::string s;
        std::cin >> s;
        for (int j = 1; j <= n; ++j) {
            if (s[j - 1] == '1') {
                g.addEdge(i, j);
            }
        }
    }
    BuildBridges(g, n);
    return 0;
}