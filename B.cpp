/*
 * B. Rick for the Weekend
 * Рику необходимо попасть на межвселенную конференцию. За каждую телепортацию он платит
 * бутылками лимонада, поэтому хочет потратить их на дорогу как можно меньше (он же всё-таки
 * на конференцию едет!). Однако после K перелетов подряд Рика начинает сильно тошнить, и
 * он ложится спать на день. Ему известны все существующие телепортации. Теперь Рик хочет
 * найти путь (наименьший по стоимости в бутылках лимонада), учитывая, что телепортация не
 * занимает времени, а до конференции осталось 10 минут (то есть он может совершить не
 * более K перелетов)!
 */
#include <iostream>
#include <vector>
#include <limits>

const int INF = std::numeric_limits<int>::max();

class Graph {
public:
    Graph() : _countOfVertex(0), _orient(false) {}

    explicit Graph(int countOfVertex, bool orient = false) : _countOfVertex(countOfVertex), _orient(orient) {
        _edge = std::vector<std::vector<std::pair<int, int>>>(countOfVertex + 1);
    }

    void addEdge(int from, int to, int cost);

    std::vector<std::pair<int, int>> getNeighbours(int vertex) const {
        return _edge[vertex];
    }

    int getSize() const {
        return _countOfVertex;
    }

private:
    int _countOfVertex;
    bool _orient;
    std::vector <std::vector<std::pair<int, int>>> _edge; // g[v][i] <-> pair(to, weight)
};

void Graph::addEdge(int from, int to, int cost) {
    _edge[from].push_back(std::make_pair(to, cost));
    if (!_orient) {
        _edge[to].push_back(std::make_pair(from, cost));
    }
}

int FindShortestPath(const Graph& g, int& k, int start, int finish) {
    std::vector<std::vector<int>> d(k + 1);
    for (int i = 0; i <= k; ++i) {
        d[i] = std::vector<int>(g.getSize() + 1, INF);
    }
    d[0][start] = 0;
    std::vector<std::pair<int,int>> neighbours;
    for (int i = 0; i < k; ++i) {
        for (int v = 1; v <= g.getSize(); ++v) {
            if (d[i][v] == INF) {
                continue;
            }
            neighbours = g.getNeighbours(v);
            for (auto u : neighbours) {
                int to = u.first, cost = u.second;
                d[i + 1][to] = std::min(d[i][v] + cost, d[i + 1][to]);
            }
        }
    }
    int result = INF;
    for (int i = 0; i <= k; ++i) {
        result = std::min(result, d[i][finish]);
    }
    return (result == INF ? -1 : result);
}

int main() {
    int n, m, k, s, f;
    std::cin >> n >> m >> k >> s >> f;
    Graph g(n, true);
    for (int i = 1; i <= m; ++i) {
        int from, to, cost;
        std::cin >> from >> to >> cost;
        g.addEdge(from, to, cost);
    }
    std::cout << FindShortestPath(g, k, s, f);
}