/*
 * E. Глупая ссора
 * Шрек и Осёл уже были на пути домой. Им оставалось только преодолеть лес, который отделял
 * их от болота. Но они поссорились, поэтому не хотят идти вместе.
 * Лес представляет собой опушки, пронумерованы числами от 1 до n и соединенные m
 * дорожками (может быть несколько дорожек соединяющих две опушки, могут быть дорожки,
 * соединяющие опушку с собой же). Из-за ссоры, если по дорожке прошел один из друзей, то
 * второй по той же дорожке уже идти не может. Сейчас друзья находятся на опушке с номером
 * s, а болото Шрека — на опушке с номером t. Помогите Шреку и Ослу добраться до болота.
 */
#include <iostream>
#include <algorithm>
#include <limits>
#include <vector>

using std::vector;
using std::pair;
using std::make_pair;

const int INF = std::numeric_limits<int>::max();

struct Edge {
    int capacity, flow;
    int id, reverseId;
    int from, to;

    explicit Edge(int from, int to, int capacity) : from(from), to(to), capacity(capacity), flow(0) {}
};

class Graph {
public:
    Graph() : _countOfVertex(0), _oriented(false) {}

    explicit Graph(int newSize, bool orientation = false) : _countOfVertex(newSize), _oriented(orientation) {
        _g = vector<vector<int>>(newSize + 1);
    }

<<<<<<< HEAD
    void addEdge(int from, int to, double rate, double commission);
=======
    void addEdge(int from, int to, int cost);

    vector<Edge>& getAllEdges() {
        return _allEdges;
    }
>>>>>>> master

    vector<int> getNeighbours(int vertex) const {
        return _g[vertex];
    }

    Edge& getEdge(int id) {
        return _allEdges[id];
    }

    int getSize() const {
        return _countOfVertex;
    }

    int getEdgeSize() const {
        return _allEdges.size();
    }

private:
    void _addEdge(int from, int to, int cost, int id, int reverseId);

    int _countOfVertex;
    bool _oriented;
    vector<vector<int>> _g;
    vector<Edge> _allEdges;
};

<<<<<<< HEAD
void Graph::addEdge(int from, int to, double rate, double commission) {
    _g[from].push_back(Edge(to, rate, commission));
    if (!_oriented) {
        _g[to].push_back(Edge(from, rate, commission));
    }
}

bool FindCycle(const Graph& g, int s, double v) {
    vector<double> mx(g.getSize() + 1, 0);
    mx[s] = v;
    vector<Edge> nb;
    for (int i = 1; i <= g.getSize(); ++i) {
        for (int curVer = 1; curVer <= g.getSize(); ++curVer) {
            nb = g.getNeighbours(curVer);
            for (auto u : nb) {
                if (mx[u.to] < (mx[curVer] - u.commission) * u.rate) {
                    mx[u.to] = (mx[curVer] - u.commission) * u.rate;
                }
=======
void Graph::addEdge(int from, int to, int cost = 1) {
    int id = _allEdges.size();
    _addEdge(from, to, cost, id, id + 1);
    _addEdge(to, from, 0, id + 1, id);
}

void Graph::_addEdge(int from, int to, int cost, int id, int reverseId) {
    Edge temp(from, to, cost);
    temp.id = id, temp.reverseId = reverseId;
    _g[from].push_back(temp.id);
    _allEdges.push_back(temp);
}

int GetFlow(Graph& g, vector<bool>& us, int v, int t, int currentFlow = INF) {
    if (v == t) {
        return currentFlow;
    }
    for (auto curEdge : g.getNeighbours(v)) {
        Edge& edge = g.getEdge(curEdge);
        Edge& revEdge = g.getEdge(edge.reverseId);
        if (!us[curEdge] && edge.capacity > 0) {
            us[curEdge] = true;
            int flow = GetFlow(g, us, edge.to, t, std::min(currentFlow, edge.capacity));
            if (flow > 0) {
                edge.capacity -= flow;
                edge.flow += flow;
                revEdge.capacity += flow;
                return flow;
>>>>>>> master
            }
        }
    }
    return 0;
}

int FindMaxFlow(Graph& g, int s, int t) {
    int maxFlow = 0;
    do {
        vector<bool> us(g.getEdgeSize() + 1, false);
        int currentFlow = GetFlow(g, us, s, t);
        if (currentFlow == 0) {
            break;
        }
        maxFlow += currentFlow;
    } while (maxFlow < 2);
    return maxFlow;
}

void GetPath(Graph& g, vector<bool>& us, vector<int>& path, int v, int t) {
    path.push_back(v);
    if (v == t) {
        return;
    } else {
        for (auto curEdge : g.getNeighbours(v)) {
            Edge& edge = g.getEdge(curEdge);
            if (!us[curEdge] && edge.flow > 0) {
                us[curEdge] = true;
                edge.flow -= 1;
                GetPath(g, us, path, edge.to, t);
                break;
            }
        }
    }
}

void Print(const vector<int>& path) {
    for (auto u : path) {
        std::cout << u << ' ';
    }
    std::cout << std::endl;
}

void FindDisjointPaths(Graph& g, int s, int t) {
    int maxFlow = FindMaxFlow(g, s, t);
    if (maxFlow < 2) {
        std::cout << "NO";
    } else {
        for (auto& edge : g.getAllEdges()) {
            Edge& revEdge = g.getEdge(edge.reverseId);
            if (edge.flow > 0 && revEdge.flow > 0) {
                int minFlow = std::min(edge.flow, revEdge.flow);
                edge.flow -= minFlow;
                revEdge.flow -= minFlow;
            }
        }
        std::cout << "YES\n";
        vector<int> path;
        vector<bool> us(g.getEdgeSize() + 1, false);
        for (int i = 1; i <= 2; ++i) {
            GetPath(g, us, path, s, t);
            Print(path);
            path.clear();
            us.assign(g.getEdgeSize() + 1, false);
        }
    }
}

int main() {
    int n, m, s, t;
    std::cin >> n >> m >> s >> t;
    Graph g(n, true);
    for (int i = 1; i <= m; ++i) {
        int from, to;
        std::cin >> from >> to;
        g.addEdge(from, to);
    }
    FindDisjointPaths(g, s, t);
    return 0;
}