#ifndef UNTITLED_LIST_H
#define UNTITLED_LIST_H

#include <iostream>

template<class T>
struct Node {
    T* value = nullptr;
    Node* next = nullptr;
    Node* prev = nullptr;

    explicit Node(const T& val) { value = new T(val); }

    explicit Node(T&& val) { value = new T(std::move(val)); }

    template<class ...Args>
    explicit Node(Args&& ...args) noexcept { value = new T(std::move(args)...); }

    Node() = default;

    ~Node() {
        delete value;
        value = nullptr;
    };

    explicit operator bool() const { return value != nullptr; }

    T& getValue() {
        if (value != nullptr)
            return *value;
        return *value;
    }

};

template<class T>
class List;

template<class T, bool isConst>
class const_no_const_iterator : public std::iterator<std::bidirectional_iterator_tag, T> {
public:
    typedef typename std::conditional<isConst, const T&, T&>::type condType;

    friend class List<T>;

    explicit const_no_const_iterator(Node<T>* node) : p(node) {}

    const_no_const_iterator() = default;

    const_no_const_iterator(const_no_const_iterator<T, true> const& it) : p(it.p) {}

    const_no_const_iterator<T, isConst>& operator=(const_no_const_iterator<T, isConst> const& other) {
        if (this != &other) {
            p = other.getPointer();
        }
        return *this;
    }

    bool operator!=(const_no_const_iterator const& other) const { return p != other.p; }

    bool operator==(const_no_const_iterator const& other) const { return p == other.p; }

    condType operator*() const { return p->getValue(); }

    const_no_const_iterator& operator++() {
        p = p->next;
        return *this;
    }

    const_no_const_iterator operator++(int) {
        const_no_const_iterator old(p);
        ++(*this);
        return old;
    }

    const_no_const_iterator& operator--() {
        p = p->prev;
        return *this;
    }

    const_no_const_iterator operator--(int) {
        const_no_const_iterator old(p);
        --(*this);
        return old;
    }

    Node<T>* getPointer() const { return p; }

    operator const_no_const_iterator<T, true>() const { return const_no_const_iterator<T, true>(p); }

    operator bool() const { return p != nullptr; }

    friend const_no_const_iterator<T, !isConst>;

private:
    Node<T>* p = nullptr;
};

template<class T>
class List {
public:
    typedef const_no_const_iterator<T, true> constIterator;
    typedef const_no_const_iterator<T, false> iterator;
    typedef std::reverse_iterator<iterator> revIterator;
    typedef T value_type;

    List() { _init(); };

    List(const List<T>& other) {
        _init();
        _assign(other.cbegin(), other.cend());
    };

    List(List<T>&& other) noexcept { _assign_move(std::move(other)); }

    List<T>& operator=(List<T> const& other);

    List<T>& operator=(List<T>&& other) noexcept;;

    ~List();

    explicit List(size_t count, const T& value = T());

    T& front() const { return _front->getValue(); }

    T& back() const { return _end->prev->getValue(); }

    iterator begin() { return iterator(_front); }

    iterator end() { return iterator(_end); }

    revIterator rbegin() { return make_revterator(end()); }

    revIterator rend() { return make_revterator(begin()); }

    constIterator cbegin() const { return constIterator(_front); }

    constIterator cend() const { return constIterator(_end); }

    [[nodiscard]] size_t size() const { return _size; }

    [[nodiscard]] bool empty() const { return _size == 0; }

    void push_back(const T& value) { _push_back(new Node(value)); }

    void push_back(T&& value) { _push_back(new Node(std::move(value))); }

    void push_front(const T& value) { _push_front(new Node(value)); }

    void push_front(T&& value) { _push_front(new Node(std::move(value))); }

    void pop_back() { erase(iterator(_end->prev)); };

    void pop_front() { erase(begin()); };

    void clear();

    void reverse();

    void unique();

    iterator insert(constIterator it, const T& value) { return _insert(it, new Node(value)); };

    iterator insert(constIterator it, T&& value);

    template<typename InputIter>
    iterator insert(constIterator it, InputIter begin, InputIter end);

    iterator erase(constIterator it);

    iterator erase(constIterator begin, constIterator end);

    template<class... Args>
    void emplace(constIterator it, Args&& ...args);

    template<class... Args>
    void emplace_front(Args&& ...args);

    template<class... Args>
    void emplace_back(Args&& ...args);

private:
    Node<T>* _front;
    Node<T>* _end;
    size_t _size = 0;

    void _push_back(Node<T>* node) { _insert(end(), node); }

    void _push_front(Node<T>* node) { _insert(begin(), node); }

    iterator _insert(constIterator it, Node<T>* node);

    void _init();

    void _assign(constIterator begin, constIterator end);

    void _assign_move(List<T>&& other);
};

template<class T>
void List<T>::clear() {
    if (empty())
        return;
    while (_front != _end) {
        --_size;
        auto toDelete = _front;
        _front = _front->next;
        delete toDelete;
    }
    _end->prev = nullptr;
    _front = _end;
}

template<class T>
void List<T>::reverse() {
    if (size() < 2)
        return;
    Node<T>* __back = _end->prev;
    for (auto node = _front; node != _end; node = node->prev) {
        std::swap(node->prev, node->next);
    }

    _end->prev = _front;
    _front->next = _end;
    _front = __back;
    _front->prev = nullptr;
}

template<class T>
List<T>::~List() {
    clear();
    delete _end;
}

template<class T>
List<T>::List(size_t count, const T& value) {
    _init();
    for (size_t i = 0; i < count; ++i) {
        push_back(value);
    }
}

template<class T>
void List<T>::unique() {
    if (size() < 2)
        return;
    for (auto node = _front; node->next != _end;) {
        if (node->getValue() == node->next->getValue()) {
            erase(iterator(node->next));
        } else {
            node = node->next;
        }
    }
}

template<class T>
typename List<T>::iterator List<T>::erase(constIterator it) {
    if (empty()) {
        return it;
    }
    --_size;
    auto next = it.getPointer()->next;
    if (it.getPointer()->prev) {
        it.getPointer()->prev->next = next;
    }
    if (next) {
        next->prev = it.getPointer()->prev;
    }
    if (it.getPointer() == _front) {
        _front = next;
    }
    delete it.getPointer();
    return iterator(next);
}

template<class T>
void List<T>::_init() {
    _end = new Node<T>();
    _front = _end;
    _size = 0;
}

template<class T>
typename List<T>::iterator List<T>::_insert(constIterator it, Node<T>* node) {
    node->next = it.getPointer();
    node->prev = it.getPointer()->prev;
    if (it.getPointer() == _front) {
        _front = node;
    } else {
        it.getPointer()->prev->next = node;
    }
    it.getPointer()->prev = node;
    ++_size;
    return iterator(node);
}

template<class T>
typename List<T>::iterator List<T>::insert(List::constIterator it, T&& value) {
    return _insert(it, new Node(std::move(value)));
}

template<class T>
template<typename InputIter>
typename List<T>::iterator List<T>::insert(constIterator it, InputIter begin, InputIter end) {
    iterator newIt;
    for (auto iter = begin; iter != end; ++iter) {
        if (iter == begin) {
            newIt = insert(it, *iter);
        } else {
            insert(it, *iter);
        }
    }
    return newIt;
}

template<class T>
typename List<T>::iterator List<T>::erase(constIterator begin, constIterator end) {
    while (begin != end) {
        auto toDelete = begin;
        ++begin;
        erase(toDelete);
    }
    return end;
}

template<class T>
template<class... Args>
void List<T>::emplace_front(Args&& ... args) {
    _insert(cbegin(), new Node<T>(std::move(args)...));
}

template<class T>
template<class... Args>
void List<T>::emplace_back(Args&& ... args) {
    _insert(cend(), new Node<T>(std::move(args)...));
}

template<class T>
List<T>& List<T>::operator=(const List<T>& other) {
    if (&other != this) {
        _assign(other.cbegin(), other.cend());
    }
    return *this;
}

template<class T>
void List<T>::_assign(List::constIterator begin, List::constIterator end) {
    clear();
    insert(cbegin(), begin, end);
}

template<class T>
template<class... Args>
void List<T>::emplace(List::constIterator it, Args&& ... args) {
    _insert(it, new Node<T>(std::move(args)...));
}

template<class T>
void List<T>::_assign_move(List<T>&& other) {
    _front = other._front;
    _end = other._end;
    _size = other._size;
    other._init();
}

template<class T>
List<T>& List<T>::operator=(List<T>&& other) noexcept {
    clear();
    delete _end;
    _assign_move(std::move(other));
    return *this;
}
#endif //UNTITLED_LIST_H

